package com.atlassian.prettyurls.api.route;

/**
 * This interface is used as an opaque key so that we can store
 * UrlRouteSets in a map and identify them by key.
 *
 * You MUST implement a suitable .equals() and .hashCode()
 *
 * @since 1.11.2
 */
public interface UrlRouteRuleSetKey {
    /**
     * @return a .hashCode() that makes sense
     */
    public int hashCode();

    /**
     * Implement .equals() properly
     */
    public boolean equals(Object obj);

}
