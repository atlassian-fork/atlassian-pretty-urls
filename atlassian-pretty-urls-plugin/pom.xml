<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.atlassian.prettyurls</groupId>
        <artifactId>atlassian-pretty-urls-parent</artifactId>
        <version>3.0.3-SNAPSHOT</version>
    </parent>

    <artifactId>atlassian-pretty-urls-plugin</artifactId>
    <name>Atlassian Pretty URLs Plugin</name>
    <description>Pretty URL support in Atlassian products</description>
    <packaging>atlassian-plugin</packaging>
    
    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.plugin</groupId>
                <artifactId>atlassian-spring-scanner-maven-plugin</artifactId>
                <version>${atlassian.scanner.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>atlassian-spring-scanner</goal>
                        </goals>
                        <phase>process-classes</phase>
                        <!-- process-classes seems to be skipped for scala -->
                    </execution>
                </executions>
                <configuration>
                    <verbose>true</verbose>
                </configuration>
            </plugin>

            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>amps-maven-plugin</artifactId>
                <extensions>true</extensions>
                <configuration>
                    <extractDependencies>false</extractDependencies>
                    <enableDevToolbox>false</enableDevToolbox>
                    <enablePde>false</enablePde>

                    <instructions>
                        <Import-Package>
                            com.atlassian.plugin.osgi.bridge.external,
                            *
                        </Import-Package>
                        <Export-Package>
                            com.atlassian.prettyurls.api.route,
                            com.atlassian.prettyurls.filter,
                            com.atlassian.prettyurls.module,
                            com.atlassian.prettyurls.obr
                        </Export-Package>
                        <Atlassian-Plugin-Key>${prettyurls.plugin.key}</Atlassian-Plugin-Key>
                        <!-- Ensure plugin is spring powered - see https://extranet.atlassian.com/x/xBS9hQ -->
                        <Spring-Context>*</Spring-Context>
                    </instructions>

                    <!-- No product config here, it would force a lot of duplication for docker.
                         For local development, use 'amps:run' in the reference plugin module instead. See README. -->
                </configuration>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <!-- Compiled in  -->
        <dependency>
            <groupId>com.atlassian.ozymandias</groupId>
            <artifactId>atlassian-plugin-point-safety</artifactId>
            <version>1.0.0</version>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugin</groupId>
            <artifactId>atlassian-spring-scanner-annotation</artifactId>
            <version>${atlassian.scanner.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.prettyurls</groupId>
            <artifactId>atlassian-pretty-urls-api</artifactId>
            <version>${project.version}</version>
        </dependency>

        <!-- Product specific deps -->    
        <dependency>
            <groupId>opensymphony</groupId>
            <artifactId>sitemesh</artifactId>
            <version>2.5-atlassian-3</version>
            <scope>provided</scope>
        </dependency>

        <!-- Platform modules -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-servlet</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-spring</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-common</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-webfragment</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>io.atlassian.fugue</groupId>
            <artifactId>fugue</artifactId>
            <scope>provided</scope>
        </dependency>

        <!--TEST DEPS-->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-library</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
        	<groupId>org.apache.httpcomponents</groupId>
        	<artifactId>httpclient</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
</project>

