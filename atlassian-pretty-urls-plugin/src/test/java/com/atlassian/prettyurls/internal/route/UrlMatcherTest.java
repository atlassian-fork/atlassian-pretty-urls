package com.atlassian.prettyurls.internal.route;

import com.atlassian.prettyurls.api.route.UrlRouteRule;
import com.sun.jersey.api.uri.UriTemplate;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.atlassian.prettyurls.internal.route.UrlMatcher.Strategy.JAX_RS_MATCHING;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


public class UrlMatcherTest {

    private UrlMatcher classUnderTest;

    @Before
    public void setUp() throws Exception {
        classUnderTest = new UrlMatcher();
    }

    @Test
    public void test_multi_rule_set_ordering_is_in_jax_rs_order() throws Exception {

        List<UrlRouteRule> rules = standardRules();

        List<UrlRouteRule> actual = JAX_RS_MATCHING.sortRules(rules);

        ArrayList<UrlRouteRule> expected = newArrayList(
                rule("/1/very/specific/non/variables"),
                rule("/1/very/specific/non/variableZ"),
                rule("/10/this/is/less/specified/"),
                rule("/20/this/is/less/specific/"),
                rule("/30/this/is/less/spec/"),
                rule("/40/this/is/less/s/"),
                rule("/1000/vars/{withvar1}/{withvar2}/{withvar3}"),
                rule("/2000/vars/{withvar1}/{withvar2}"),
                rule("/3000/vars/{withvar1}")
        );

        assertEquals(actual, expected);

    }

    @Test
    public void test_multi_rule_set_matching_happens_as_expected() throws Exception {

        UrlMatcher.Result result;

        result = classUnderTest.getMatchingRule(
                "", standardRules(), JAX_RS_MATCHING);
        assertThat(result.matches(), equalTo(false));

        result = classUnderTest.getMatchingRule(
                "/1/very/specific/non/variableX", standardRules(), JAX_RS_MATCHING);
        assertThat(result.matches(), equalTo(false));

        result = classUnderTest.getMatchingRule(
                "/1/very/specific/non/variableZ", standardRules(), JAX_RS_MATCHING);
        assertThat(result.matches(), equalTo(true));

        result = classUnderTest.getMatchingRule(
                "/2000/vars/v1/v2", standardRules(), JAX_RS_MATCHING);

        assertThat(result.matches(), equalTo(true));
        assertThat(result.getParsedVariableValues().get("withvar1"), equalTo("v1"));
        assertThat(result.getParsedVariableValues().get("withvar2"), equalTo("v2"));
    }

    @Test
    public void test_single_ruleset_in_legacy_list_order() throws Exception {
        /*
          * We need to not use JAX-RS ordering because of legacy compatibility.  Since we have
          * plenty of pretty url code out there that assumed "list" order then we must preserve it.
          *
          * So this means that capturing groups (aka variables) can precede explicit chars if they are
          * placed in list order first.  While not ideal (on reflection) is preserves backwards compatibility
          * for those that might have accidentally relied on it
          *
          * We only need this for "single rule sets" per top level path
         */

        List<UrlRouteRule> rules = newArrayList(
                rule("/in/multi/set/this/would/be/{last}"),
                rule("/in/multi/set/this/would/be/first")
        );

        UrlMatcher.Result result = classUnderTest.getMatchingRule(
                "/in/multi/set/this/would/be/first", rules, UrlMatcher.Strategy.LIST_ORDER_MATCHING);

        assertThat(result.matches(), equalTo(true));
        assertThat(result.getParsedVariableValues().get("last"), equalTo("first"));
    }

    private List<UrlRouteRule> standardRules() {
        final List<UrlRouteRule> rules = newArrayList();

        // when they are the same length with no variables they end up as a string comparison
        rules.addAll(shuffled(
                "/1/very/specific/non/variableZ",
                "/1/very/specific/non/variables"
        ));

        rules.addAll(shuffled(
                "/40/this/is/less/s/",
                "/10/this/is/less/specified/",
                "/30/this/is/less/spec/",
                "/20/this/is/less/specific/"
        ));

        rules.addAll(shuffled(
                "/2000/vars/{withvar1}/{withvar2}",
                "/1000/vars/{withvar1}/{withvar2}/{withvar3}",
                "/3000/vars/{withvar}"
        ));
        return rules;
    }

    /**
     * A shuffled set of rules truly proves that initial order does not matter
     *
     * @param path  the 1st path
     * @param paths more paths
     *
     * @return a shuffled list
     */
    private List<UrlRouteRule> shuffled(String path, String... paths) {
        List<UrlRouteRule> rules = rules(path, paths);
        Collections.shuffle(rules);
        return rules;
    }

    private List<UrlRouteRule> rules(String path, String... paths) {
        List<UrlRouteRule> rules = newArrayList();
        rules.add(rule(path));
        for (String s : paths) {
            rules.add(rule(s));
        }
        return rules;
    }

    private UrlRouteRule rule(String path) {
        return new UrlRouteRule(new UriTemplate(path), new UriTemplate("to"), newArrayList("GET"), UrlRouteRule.ParameterMode.PASS_ALL);
    }

}