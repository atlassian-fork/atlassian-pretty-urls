package com.atlassian.prettyurls.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * A filter that can REDIRECTS from pretty urls back to the other urls when told to by {@link PrettyUrlsMatcherFilter}
 */
public class PrettyUrlsDispatcherFilter extends PrettyUrlsCommonFilter {
    private static final Logger log = LoggerFactory.getLogger(PrettyUrlsDispatcherFilter.class);


    public PrettyUrlsDispatcherFilter() {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;

        /*
          This code presupposes VERY STRONGLY that the PrettyUrlsMatcherFilter has already been run and that it has put attributes
          in the request to decide if it should be routed or not.  We could evaluate the rules again BUT WHY?

          This is light weight and suffers in design only that the previous filter MUST have run.  Which this plugin controls!
         */

        if (httpServletRequest.getAttribute(PrettyUrlsCommonFilter.PRETTY_URLS_PERFORM_ROUTE) != null) {
            final String fromURI = (String) httpServletRequest.getAttribute(PrettyUrlsCommonFilter.PRETTY_URLS_FROM_URI);
            final String toURI = (String) httpServletRequest.getAttribute(PrettyUrlsCommonFilter.PRETTY_URLS_TO_URI);

            // prevent the dispatcher from acting again!
            httpServletRequest.removeAttribute(PrettyUrlsCommonFilter.PRETTY_URLS_PERFORM_ROUTE);

            if (log.isDebugEnabled()) {
                log.debug("Routing {} ==> {}", fromURI, toURI);
            }

            //
            // The original request parameters (POST data and what not) are preserved by the dispatcher process.
            // It handles the fact that original parameters are moved onto the redirect INCLUDING any new
            // parameters in the routed toURI.  Cool eh?
            //
            httpServletRequest.getRequestDispatcher(toURI).forward(servletRequest, servletResponse);
            return;
        }
        //
        // move along folks - nothing to see here
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
