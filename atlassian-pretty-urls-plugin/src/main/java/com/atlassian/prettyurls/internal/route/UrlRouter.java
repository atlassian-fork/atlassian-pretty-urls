package com.atlassian.prettyurls.internal.route;

import com.atlassian.plugin.servlet.filter.FilterLocation;

import javax.servlet.http.HttpServletRequest;

/**
 * This can route from a pretty URL to an action URL
 */
public interface UrlRouter {
    /**
     * This will return a mapped URL based routing rules in play OR null if a mapping cannot
     * be found.
     *
     * @param httpRequest    the request in play
     * @param filterLocation the location of the plugin filter
     * @return a mapped URL
     */
    Result route(HttpServletRequest httpRequest, FilterLocation filterLocation);

    /**
     * The result of a route operation
     */
    interface Result {
        String toURI();

        boolean isRouted();
    }

}
