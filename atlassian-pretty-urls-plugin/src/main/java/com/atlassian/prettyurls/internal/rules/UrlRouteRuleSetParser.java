package com.atlassian.prettyurls.internal.rules;

import com.atlassian.plugin.servlet.filter.FilterLocation;
import com.atlassian.prettyurls.api.route.DefaultUrlRouteRuleSetKey;
import com.atlassian.prettyurls.api.route.RoutePredicate;
import com.atlassian.prettyurls.api.route.UrlRouteRule;
import com.atlassian.prettyurls.api.route.UrlRouteRuleSet;
import com.atlassian.prettyurls.internal.util.UrlUtils;
import com.google.common.collect.Lists;
import com.sun.jersey.api.uri.UriTemplate;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.atlassian.prettyurls.api.route.UrlRouteRule.ParameterMode.PASS_UNMAPPED;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Parses out the {@link com.atlassian.prettyurls.api.route.UrlRouteRuleSet} for a given plugin from its module
 * descriptor XML
 */
public class UrlRouteRuleSetParser {
    private static final Logger log = LoggerFactory.getLogger(UrlRouteRuleSetParser.class);
    private static final ArrayList<String> HEAD = Lists.newArrayList("HEAD");
    private static final ArrayList<String> GET = Lists.newArrayList("GET");
    private static final ArrayList<String> POST = Lists.newArrayList("POST");
    private static final ArrayList<String> PUT = Lists.newArrayList("PUT");
    private static final ArrayList<String> DELETE = Lists.newArrayList("DELETE");
    private static final ArrayList<String> OPTIONS = Lists.newArrayList("OPTIONS");
    private static final ArrayList<String> PATCH = Lists.newArrayList("PATCH");

    /**
     * This maps between the underlying plugin system element conditions
     */
    public interface PredicateMaker {
        RoutePredicate<UrlRouteRuleSet> makeRuleSetPredicate(Element routing);

        RoutePredicate<UrlRouteRule> makeRulePredicate(Element route);
    }


    public UrlRouteRuleSet parse(String moduleKey, Element element, final FilterLocation location, PredicateMaker predicateMaker) {
        checkNotNull(moduleKey);
        checkNotNull(element);

        UrlRouteRuleSet.Builder builder = new UrlRouteRuleSet.Builder()
                .setKey(new DefaultUrlRouteRuleSetKey(moduleKey))
                .setLocation(location);

        String path = element.attributeValue("path", "").trim();
        if (!path.isEmpty()) {
            if (validatePath(path)) {
                builder.addTopLevelPath(UrlUtils.startWithSlash(path));
            } else {
                log.error("'" + path + "' is not an acceptable top level path for URL routing.");
                return null;
            }
        } else {
            log.error("You must provide a path attribute in order to get URL routing.");
            return null;
        }

        builder.setPredicate(predicateMaker.makeRuleSetPredicate(element));

        // standard route instruction
        parseRules("route", element, builder, predicateMaker, path, Collections.<String>emptyList());

        // the following are syntactic sugar eg.
        //
        //    <get to='' from=''/> is the same as
        //    <route to='' from='' verbs='get' />
        //
        parseRules("head", element, builder, predicateMaker, path, HEAD);
        parseRules("get", element, builder, predicateMaker, path, GET);
        parseRules("post", element, builder, predicateMaker, path, POST);
        parseRules("put", element, builder, predicateMaker, path, PUT);
        parseRules("delete", element, builder, predicateMaker, path, DELETE);
        parseRules("options", element, builder, predicateMaker, path, OPTIONS);
        parseRules("patch", element, builder, predicateMaker, path, PATCH);

        return builder.build();
    }

    private void parseRules(String elementName, Element element, UrlRouteRuleSet.Builder builder, final PredicateMaker predicateMaker, String path, List<String> providedHttpVerbs) {
        //noinspection unchecked
        List<Element> elements = element.elements(elementName);
        for (Element e : elements) {
            String fromStr = e.attributeValue("from");
            String toStr = e.attributeValue("to", "").trim();
            RoutePredicate<UrlRouteRule> routePredicate = predicateMaker.makeRulePredicate(e);

            if (toStr.isEmpty()) {
                log.error("Encountered blank to=\"\" rule.  Ignoring it...");
                continue;
            }
            if (fromStr == null) {
                log.error("Missing from=\"\" rule.  Ignoring it...");
                continue;
            }
            UriTemplate from;
            if (fromStr.trim().isEmpty()) {
                // ok this is the case where they want to map directly from the top level path
                from = createURI(path);
            } else {
                from = createURI(path, fromStr);
            }


            // to destination does not get the prepended top level path
            UriTemplate to = createURI("", toStr);

            List<String> httpVerbs = providedHttpVerbs;
            if (httpVerbs.isEmpty()) {
                httpVerbs = parseHttpVerbs(e);
            }

            if (from != null && to != null) {
                builder.addRule(from, to, httpVerbs, routePredicate, PASS_UNMAPPED);
            }
        }
    }

    private List<String> parseHttpVerbs(Element e) {
        ArrayList<String> httpVerbs = Lists.newArrayList();
        String verbs = e.attributeValue("verbs", "");
        String[] split = verbs.split(",");
        for (String verb : split) {
            verb = verb.trim();
            if (!verb.isEmpty()) {
                httpVerbs.add(verb.toUpperCase());
            }
        }
        return httpVerbs;
    }

    private boolean validatePath(String path) {
        return !path.equals("/");
    }

    private UriTemplate createURI(String path, String uriStr) {
        String template = UrlUtils.prependPath(path, uriStr);
        try {
            return new UriTemplate(template);
        } catch (IllegalArgumentException e) {
            log.error("Unable to parse routing URI " + String.valueOf(template));
            return null;
        }
    }

    private UriTemplate createURI(String uriStr) {
        try {
            return new UriTemplate(uriStr);
        } catch (IllegalArgumentException e) {
            log.error("Unable to parse routing URI " + String.valueOf(uriStr));
            return null;
        }
    }
}
